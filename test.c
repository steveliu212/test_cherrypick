#include <stdio.h>

void func_b(void)
{
	printf("func b is called\n");
}

void func_a(void)
{
	printf("func a is called\n");
}

int main(int argc, char *argv[])
{
	func_a();
	func_b();

	return 0;
}
